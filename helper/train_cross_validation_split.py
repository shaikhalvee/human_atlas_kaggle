import pandas as pd
import time
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import cv2


root_path = "E:/python projects/human atlas"
split_ratio = .8  # 80% training data


def split_dataset(table, max_split_ratio):
    train = table.sample(frac=max_split_ratio, random_state=int(time.time()))
    test = table.drop(train.index)

    train = train.reset_index().drop(columns='index')
    test = test.reset_index().drop(columns='index')

    print(np.sum(train['Rods & rings'] == 1))

    x = table.iloc[:, 1:].sum(axis=0).sort_values(ascending=False)
    plt.figure(figsize=(15, 15))
    sns.barplot(x=x.values, y=x.index.values)
    plt.title("Data Set")

    x = train.iloc[:, 1:].sum(axis=0).sort_values(ascending=False)
    plt.figure(figsize=(15, 15))
    sns.barplot(x=x.values, y=x.index.values)
    plt.title("Train Set")

    x = test.iloc[:, 1:].sum(axis=0).sort_values(ascending=False)
    plt.figure(figsize=(15, 15))
    sns.barplot(x=x.values, y=x.index.values)
    plt.title("Test Set")
    plt.show()

    return train, test


def process_and_move_files(df, dest):
    for filename in df['Id'].values:
        print('saving file {}'.format(filename))
        img_r = cv2.imread(root_path + '/raw_data/train/' + filename + '_red.png', 0)
        img_g = cv2.imread(root_path + '/raw_data/train/' + filename + '_green.png', 0)
        img_b = cv2.imread(root_path + '/raw_data/train/' + filename + '_blue.png', 0)
        img_y = cv2.imread(root_path + '/raw_data/train/' + filename + '_yellow.png', 0)

        img_r2 = img_r.astype(np.uint16) * img_g.astype(np.uint16)
        img_b2 = img_b.astype(np.uint16) * img_g.astype(np.uint16)
        img_y2 = img_y.astype(np.uint16) * img_g.astype(np.uint16)

        # normalize
        img_r2 = (img_r2 / np.max(img_r2) * 255).astype(np.uint8)
        img_b2 = (img_b2 / np.max(img_b2) * 255).astype(np.uint8)
        img_y2 = (img_y2 / np.max(img_y2) * 255).astype(np.uint8)

        cv2.imwrite(dest + '/' + filename + '_red.png', img_r2)
        cv2.imwrite(dest + '/' + filename + '_blue.png', img_b2)
        cv2.imwrite(dest + '/' + filename + '_yellow.png', img_y2)
        cv2.imwrite(dest + '/' + filename + '_green.png', img_g)
    return None


def main():
    desired_width = 520
    pd.set_option('display.width', desired_width)
    pd.set_option('display.max_columns', 30)

    table = pd.read_csv(root_path + "/organized_train.csv")

    while True:
        train, cross_val = split_dataset(table=table, max_split_ratio=split_ratio)

        save = input("save data")

        if save == 'y':
            train.to_csv(root_path+'/train.csv')
            cross_val.to_csv(root_path+'/cross_val.csv')
            process_and_move_files(train, dest=root_path+'/gen_data/train')
            process_and_move_files(cross_val, dest=root_path+'/gen_data/cross_val')
            break


if __name__ == "__main__":
    main()
