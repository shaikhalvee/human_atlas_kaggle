# To reduce time during training save png images as torch tensor
import numpy as np
import torch
import cv2
import pandas as pd
import os


root_path = root_path = "E:/python projects/human atlas"


def main():
    train = pd.read_csv(root_path + '/train.csv')
    cross_val = pd.read_csv(root_path + '/cross_val.csv')

    # convert training images
    for filename in train['Id'].values:
        img_r = cv2.imread(root_path + '/gen_data/train/' + filename + '_red.png', 0)
        img_g = cv2.imread(root_path + '/gen_data/train/' + filename + '_green.png', 0)
        img_b = cv2.imread(root_path + '/gen_data/train/' + filename + '_blue.png', 0)
        img_y = cv2.imread(root_path + '/gen_data/train/' + filename + '_yellow.png', 0)

        img = np.stack([img_g, img_r, img_b, img_y], axis=2)
        torch_img = torch.from_numpy(img)
        torch.save(torch_img, root_path + '/gen_data/train/' + filename + '.pt')

        os.remove(root_path + '/gen_data/train/' + filename + '_red.png')
        os.remove(root_path + '/gen_data/train/' + filename + '_green.png')
        os.remove(root_path + '/gen_data/train/' + filename + '_blue.png')
        os.remove(root_path + '/gen_data/train/' + filename + '_yellow.png')

        del img_r, img_g, img_b, img_y, img, torch_img

    # convert cross validation images
    for filename in cross_val['Id'].values:
        img_r = cv2.imread(root_path + '/gen_data/cross_val/' + filename + '_red.png', 0)
        img_g = cv2.imread(root_path + '/gen_data/cross_val/' + filename + '_green.png', 0)
        img_b = cv2.imread(root_path + '/gen_data/cross_val/' + filename + '_blue.png', 0)
        img_y = cv2.imread(root_path + '/gen_data/cross_val/' + filename + '_yellow.png', 0)

        img = np.stack([img_g, img_r, img_b, img_y], axis=2)
        torch_img = torch.from_numpy(img)
        torch.save(torch_img, root_path + '/gen_data/cross_val/' + filename + '.pt')

        os.remove(root_path + '/gen_data/cross_val/' + filename + '_red.png')
        os.remove(root_path + '/gen_data/cross_val/' + filename + '_green.png')
        os.remove(root_path + '/gen_data/cross_val/' + filename + '_blue.png')
        os.remove(root_path + '/gen_data/cross_val/' + filename + '_yellow.png')

        del img_r, img_g, img_b, img_y, img, torch_img


if __name__ == '__main__':
    main()
