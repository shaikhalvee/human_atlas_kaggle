import matplotlib.pyplot as plt
import numpy as np
import cv2
import pandas as pd


root_path = "E:/python projects/human atlas"


def show_image(img_name, labels):

    img_r = cv2.imread(root_path + "/data/train/" + img_name + "_red.png", 0)
    img_g = cv2.imread(root_path + "/data/train/" + img_name + "_green.png", 0)
    img_b = cv2.imread(root_path + "/data/train/" + img_name + "_blue.png", 0)
    img_y = cv2.imread(root_path + "/data/train/" + img_name + "_yellow.png", 0)

    # multiply each channel by green channel to see the difference
    img_r2 = img_r.astype(np.uint16) * img_g.astype(np.uint16)
    img_b2 = img_b.astype(np.uint16) * img_g.astype(np.uint16)
    img_y2 = img_y.astype(np.uint16) * img_g.astype(np.uint16)

    # normalize
    img_r2 = (img_r2 / np.max(img_r2) * 255).astype(np.uint8)
    img_b2 = (img_b2 / np.max(img_b2) * 255).astype(np.uint8)
    img_y2 = (img_y2 / np.max(img_y2) * 255).astype(np.uint8)

    # histogram calculation for before after multiplication of each channel
    ranges = [0, 256]
    hist_img_r = cv2.calcHist(images=[img_r], channels=[0], mask=None, histSize=[256], ranges=ranges)
    hist_img_b = cv2.calcHist(images=[img_b], channels=[0], mask=None, histSize=[256], ranges=ranges)
    hist_img_y = cv2.calcHist(images=[img_y], channels=[0], mask=None, histSize=[256], ranges=ranges)
    hist_img_r2 = cv2.calcHist(images=[img_r2], channels=[0], mask=None, histSize=[256], ranges=ranges)
    hist_img_b2 = cv2.calcHist(images=[img_b2], channels=[0], mask=None, histSize=[256], ranges=ranges)
    hist_img_y2 = cv2.calcHist(images=[img_y2], channels=[0], mask=None, histSize=[256], ranges=ranges)

    # plot
    ylim_up = 500
    plt.figure(img_name)
    plt.suptitle(str(labels))
    plt.subplot(331)
    plt.imshow(img_r2)
    plt.subplot(332)
    plt.imshow(img_r)
    plt.title('Microtubules Channel')
    plt.subplot(333)
    plt.plot(hist_img_r, color='r')
    plt.plot(hist_img_r2, color='g')
    plt.ylim(0, ylim_up)

    plt.subplot(334)
    plt.imshow(img_b2)
    plt.subplot(335)
    plt.imshow(img_b)
    plt.title('Nucleus Channel')
    plt.subplot(336)
    plt.plot(hist_img_b, color='r')
    plt.plot(hist_img_b2, color='g')
    plt.ylim(0, ylim_up)

    plt.subplot(337)
    plt.imshow(img_y2)
    plt.subplot(338)
    plt.imshow(img_y)
    plt.title('Endoplasmic Reticulum Channel')
    plt.subplot(339)
    plt.plot(hist_img_y, color='r')
    plt.plot(hist_img_y2, color='g')
    plt.ylim(0, ylim_up)

    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()
    plt.show()
    plt.close("all")


def cycle_through_images(df):
    df_len = df.shape[0]
    for i in range(df_len):
        show_image(df.loc[i, 'Id'], list(df.keys()[df.loc[i, :] == 1]))


def main():
    table = pd.read_csv(root_path+"/organized_train.csv")
    cycle_through_images(table.sample(frac=1.0).reset_index().drop(columns='index'))


if __name__ == '__main__':
    main()
